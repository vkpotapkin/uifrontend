import './App.css';
import axios from 'axios'
import React, { useEffect, useState } from 'react';
import {Content} from "./Content";

function App() {

    let axiosAjax = axios.create()
    let axiosResponse = "";

    var [appState, setAppState] = useState();

    function getLanguageFromServer() {
        let url = "static/getPreferredLocale"

        axiosAjax.post(url).then((response) => {
            axiosResponse = response.data;
            setAppState(axiosResponse);
        });
    }

  return (
    <div className="App">
      <header className="App-header">
          Hello World UI
          <br/>
          <br/>
          <Content/>
      </header>
    </div>
  );
}

export default App;
