import React, { Component } from 'react'
import axios from "axios";

export class Content extends React.Component {
    constructor(props) {
        super(props)
        this.state = {textValue: ''}
    }

    getContent = (url) => {
        axios.post(url).then((response) => {
            this.setState({
                textValue: JSON.stringify(response.data, null, 2)
            })
        })
    };

    render () {
        return (
            <div>
                <button onClick={() => this.getContent("/getContent")}>Получить данные с сервера</button>
                <br/><br/>
                <textarea rows={3} cols={50} {...this.props} placeholder={this.state.textValue}/>
            </div>
        )
    }
}